# gitdab

Gitdab's scripts, icons and gitea customizations

We run a fork of Gitea v1.6 with following code changes:
- Repo migrations are disabled to prevent leaking server's IP address.
- The description on embeds is changed.

Various other changes are also applied to templates.

## License

All original code is GPLv3-only.

All changes to Gitea code uses same license as Gitea.

All rights are reserved on the images unless specified otherwise.

## Credits

- Luna: Idea for Gitdab
- Slice (sliceofcode): Gitdab logo
